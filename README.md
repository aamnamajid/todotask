# ToDoTask

created a RESTful todo list API with endpoints that will 
- create a task
- get or read list of all tasks
- read a particular task
- delete a task
- update a task
